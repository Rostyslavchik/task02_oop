package com.rostyslavprotsiv.model.entity;

import java.util.List;
import java.util.Objects;

public class Shop {
    private List<PlumbTool> PlumbTools;

    public Shop() {
    }

    public Shop(List<PlumbTool> plumbTools) {
        PlumbTools = plumbTools;
    }

    public List<PlumbTool> getPlumbTools() {
        return PlumbTools;
    }

    public void setPlumbTools(List<PlumbTool> plumbTools) {
        PlumbTools = plumbTools;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)  {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Shop shop = (Shop) o;
        return Objects.equals(PlumbTools, shop.PlumbTools);
    }

    @Override
    public int hashCode() {
        return Objects.hash(PlumbTools);
    }

    @Override
    public String toString() {
        return "Shop{"
                + "PlumbTools="
                + PlumbTools
                + '}';
    }
}
