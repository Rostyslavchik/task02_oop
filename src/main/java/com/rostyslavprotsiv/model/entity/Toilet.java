package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exception.PlumbToolLogicalException;

import java.util.Objects;

/**
 * {@code Toilet} class represents toilet unit in a store.
 *
 * @version 1.0.0
 * @author Rostyslav Protsiv
 * @since 09.08.2021
 */
public class Toilet extends PlumbTool {
    private int weight;

    public Toilet(double price) throws PlumbToolLogicalException {
        super(price);
    }

    public Toilet(double price, int weight) throws PlumbToolLogicalException {
        super(price);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean checkTool() {
        return weight > 68;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Toilet toilet = (Toilet) o;
        return weight == toilet.weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight);
    }

    @Override
    public String toString() {
        return super.toString()
                + ", weight= " + weight;
    }
}
