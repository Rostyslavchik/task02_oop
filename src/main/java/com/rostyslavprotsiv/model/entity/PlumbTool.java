package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exception.PlumbToolLogicalException;

import java.util.Objects;

public abstract class PlumbTool {
    private double price;

    public PlumbTool(double price) throws PlumbToolLogicalException {
        if (checkPrice(price)) {
            this.price = price;
        } else {
            throw new PlumbToolLogicalException();
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) throws PlumbToolLogicalException {
        if (checkPrice(price)) {
            this.price = price;
        } else {
            throw new PlumbToolLogicalException();
        }
    }

    public abstract boolean checkTool();

    private boolean checkPrice(double price) {
        return price > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlumbTool plumbTool = (PlumbTool) o;
        return Double.compare(plumbTool.price, price) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price);
    }

    @Override
    public String toString() {
        return getClass().getName() + " price=" + price;
    }
}
