package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exception.PlumbToolLogicalException;

public class Sink extends PlumbTool {
    private int size;
    private String company;

    public Sink(double price) throws PlumbToolLogicalException {
        super(price);
    }

    public Sink(double price, int size, String company)
                    throws PlumbToolLogicalException {
        super(price);
        this.size = size;
        this.company = company;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean checkTool() {
        return company.length() > 6;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        Sink other = (Sink) obj;
        if (size != other.size) {
            return false;
        }
        if (company == null) {
            if (other.company != null) {
                return false;
            }
        } else if (!company.equals(other.company)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int) (super.hashCode() + size * 34.32
                        + ((company == null) ? 0 : company.hashCode()));
    }

    @Override
    public String toString() {
        return super.toString() + ", size= " + size + ", company: " + company;
    }
}
