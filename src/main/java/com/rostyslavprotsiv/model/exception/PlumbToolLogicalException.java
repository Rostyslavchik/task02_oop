package com.rostyslavprotsiv.model.exception;

public class PlumbToolLogicalException extends Exception {
    public PlumbToolLogicalException() {
    }

    public PlumbToolLogicalException(String s) {
        super(s);
    }

    public PlumbToolLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PlumbToolLogicalException(Throwable throwable) {
        super(throwable);
    }
}
