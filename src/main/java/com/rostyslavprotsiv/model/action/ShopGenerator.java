package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.PlumbTool;
import com.rostyslavprotsiv.model.entity.Shop;
import com.rostyslavprotsiv.model.entity.Sink;
import com.rostyslavprotsiv.model.entity.Toilet;
import com.rostyslavprotsiv.model.exception.PlumbToolLogicalException;

import java.util.LinkedList;
import java.util.List;

public class ShopGenerator {
    public Shop generateFromDB() throws PlumbToolLogicalException {
        Sink sink = new Sink(10, 12, "aaa");
        Sink sink1 = new Sink(11, 34, "aava");
        Sink sink2 = new Sink(12, 54, "axaa");
        Toilet toilet = new Toilet(13, 38);
        Toilet toilet1 = new Toilet(45, 85);
        Toilet toilet2 = new Toilet(65, 34);
        List<PlumbTool> tools = new LinkedList<>();
        tools.add(sink1);
        tools.add(sink2);
        tools.add(toilet);
        tools.add(sink);
        tools.add(toilet1);
        tools.add(toilet2);
        return new Shop(tools);
    }
}
