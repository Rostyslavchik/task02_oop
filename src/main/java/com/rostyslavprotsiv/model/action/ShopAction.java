package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.PlumbTool;
import com.rostyslavprotsiv.model.entity.Shop;
import com.rostyslavprotsiv.model.entity.Sink;

import java.util.LinkedList;
import java.util.List;

public class ShopAction {
    public PlumbTool generateOffer(Shop shop, String name, double price) {
        List<PlumbTool> units = shop.getPlumbTools();
        for (PlumbTool unit: units) {
            if (unit.getClass().getSimpleName().equals(name)
                    && unit.getPrice() == price) {
                return unit;
            }
        }
        return null;
    }

    public List<PlumbTool> findSinksCheaperThan(Shop shop, double price) {
        List<PlumbTool> units = shop.getPlumbTools();
        List<PlumbTool> newUnits = new LinkedList<>();
        for (PlumbTool unit: units) {
            if (unit instanceof Sink && unit.getPrice() < price) {
                newUnits.add(unit);
            }
        }
        return newUnits;
    }
}
