/**
 * <h2>This module is representation of View abstraction in MVC.</h2>
 *
 * @version 1.0.0
 * @author Rostyslav Protsiv
 * @since 10.08.2021
 */

package com.rostyslavprotsiv.view;
