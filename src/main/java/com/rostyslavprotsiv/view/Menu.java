package com.rostyslavprotsiv.view;

import java.util.Scanner;

public final class Menu {
    public static final String ENCODING = "UTF-8";
    public static final Scanner SCAN = new Scanner(System.in, ENCODING);
    public static final String CLASS_PATH =
            "com.rostyslavprotsiv.model.entity.";

    public int askAboutMethod() {
        int method;
        System.out.println("Hi, you can choose what you want to do : ");
        System.out.println("    1. Check if there is some unit "
                + "with specified price");
        System.out.println("    2. Find sink that you will be able to buy");
        System.out.println("Please enter the number of operation you prefer");
        method = SCAN.nextInt();
        if (method != 1 && method != 2) {
            throw new IllegalArgumentException();
        } else {
            return method;
        }
    }

    public String askToInputToolName() {
        System.out.println("Please, input the unit name:");
        SCAN.nextLine();       //consuming the <enter> from input above
        return SCAN.nextLine();
    }

    public double askToInputToolPrice() {
        System.out.println("Please, input the unit price:");
        return SCAN.nextDouble();
    }

    public void printGeneratedOffer(String plumbTool) {
        System.out.println("Your offer is generated : " + plumbTool);
    }

    public void printSinksCheaperThan(String sinks, double price) {
        System.out.println("Sinks that are cheaper, than " + price
                + " are : \n" + sinks.replaceAll(CLASS_PATH, "\n"));
    }

    public void informAboutBadInput() {
        System.out.println("You input bad values");
    }
}
