package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.ShopAction;
import com.rostyslavprotsiv.model.action.ShopGenerator;
import com.rostyslavprotsiv.model.entity.PlumbTool;
import com.rostyslavprotsiv.model.entity.Shop;
import com.rostyslavprotsiv.model.exception.PlumbToolLogicalException;
import com.rostyslavprotsiv.view.Menu;

import java.util.List;

public class ShopController {
    private static final Menu VIEW = new Menu();
    private static final ShopAction MODEL = new ShopAction();

    public void executeFirstPoint(Shop shop) {
        String unitName = VIEW.askToInputToolName();
        double price = VIEW.askToInputToolPrice();
        PlumbTool tool = MODEL.generateOffer(shop, unitName, price);
        if (tool != null) {
            VIEW.printGeneratedOffer(tool.toString());
        } else {
            VIEW.informAboutBadInput();
        }
    }

    public void executeSecondPoint(Shop shop) {
        double price = VIEW.askToInputToolPrice();
        List<PlumbTool> tools = MODEL.findSinksCheaperThan(shop, price);
        VIEW.printSinksCheaperThan(tools.toString(), price);
    }

    public void execute() throws PlumbToolLogicalException,
                                 IllegalArgumentException {
        ShopGenerator generator = new ShopGenerator();
        Shop shop = generator.generateFromDB();
        int method = VIEW.askAboutMethod();
        if (method == 1) {
            executeFirstPoint(shop);
        } else if (method == 2) {
            executeSecondPoint(shop);
        }
    }
}
