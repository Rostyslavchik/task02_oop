/**
 * <h1>This module is a representation of Controller abstraction in MVC.</h1>
 *
 * @version 1.0.0
 * @author Rostyslav Protsiv
 * @since 10.08.2021
 */

package com.rostyslavprotsiv.controller;
