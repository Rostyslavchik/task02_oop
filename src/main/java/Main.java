import com.rostyslavprotsiv.controller.ShopController;
import com.rostyslavprotsiv.model.exception.PlumbToolLogicalException;

public class Main {
    public static void main(String[] args) {
        ShopController controller = new ShopController();
        try {
            controller.execute();
        } catch (PlumbToolLogicalException | IllegalArgumentException e) {
            e.printStackTrace();
        }

    }
}
